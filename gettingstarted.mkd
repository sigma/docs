Title: Getting Started
CSS: /css/main.css

#{include head}

# Getting Started

## Stages
Currently there are stages available for the following platforms:

- [x86_64-pc-linux-gnu, from dev.exherbo.org](//dev.exherbo.org/stages/exherbo-x86_64-pc-linux-gnu-current.tar.xz)
- [i686-pc-linux-gnu, from dev.exherbo.org](//dev.exherbo.org/stages/exherbo-i686-pc-linux-gnu-current.tar.xz)

### Experimental
These are stages targeting fairly new targets, or ones that do not have a lot of users.

Users should be cautious of using these targets for daily systems and expect issues using them for critical things.

- [x86_64-pc-linux-musl, from dev.exherbo.org](//dev.exherbo.org/stages/exherbo-x86_64-pc-linux-musl-current.tar.xz)

### Stage Requirements
* A Linux kernel \>= v4.4
* [XZ utils](http://tukaani.org/xz)

Easiest solution: [SystemRescueCD](http://www.sysresccd.org/Download)

## Install Guide
[Installing Exherbo](install-guide.html)

## Managing packages
* [Paludis documentation](http://paludis.exherbo.org/)
* [Frequently Asked Questions (FAQ)](/docs/faq.html)

## Contributing
[Contributing to Exherbo](contributing.html)

## Source code
It is possible to get all of our shiny stuff via anonymous git.

You can browse our repositories via [cgit](//git.exherbo.org).

#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
